package com.example.android.miwok.models;

public class TranslationModel {

    public int defaultTranslationId;
    public int miwokTranslationId;
    public int categoryId = -1;
    public int drawableId = -1;

    public TranslationModel(int defaultTranslationId, int miwokTranslationId, int categoryId) {
        this.defaultTranslationId = defaultTranslationId;
        this.miwokTranslationId = miwokTranslationId;
        this.categoryId = categoryId;
    }

    public TranslationModel(int defaultTranslationId, int miwokTranslationId, int categoryId, int drawableId) {
        this.defaultTranslationId = defaultTranslationId;
        this.miwokTranslationId = miwokTranslationId;
        this.categoryId = categoryId;
        this.drawableId = drawableId;
    }

    public int getDefaultTranslationId() {
        return this.defaultTranslationId;
    }

    public int getMiwokTranslationId() {
        return this.miwokTranslationId;
    }

    public int getCategoryId() {
        return this.categoryId;
    }

    public int getDrawableId() {
        return this.drawableId;
    }

    public boolean hasImage() {
        return this.getDrawableId() > -1;
    }

}
