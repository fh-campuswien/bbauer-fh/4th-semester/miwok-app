package com.example.android.miwok;

import android.provider.BaseColumns;

public final class DatabaseContract {

    public static final int DATABASE_VERSION = 5;
    public static final String DATABASE_NAME = "miwok.db";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    private DatabaseContract() {
    }

    public static abstract class TranslationsTable implements BaseColumns {
        public static final String TABLE_NAME = "translations";
        public static final String DEFAULT_COL1 = "defaultTranslationRessourceId";
        public static final String MIWOK_COL2 = "miwokTranslationRessourceId";
        public static final String IMAGE_ID_COL3 = "imageRessourceId";
        public static final String CATEGORY_ID_COL4 = "categoryId";


        public static final String CREATE_TABLE = "CREATE TABLE " +
                TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                DEFAULT_COL1 + INTEGER_TYPE + COMMA_SEP +
                MIWOK_COL2 + INTEGER_TYPE + COMMA_SEP +
                IMAGE_ID_COL3 + INTEGER_TYPE + COMMA_SEP +
                CATEGORY_ID_COL4 + INTEGER_TYPE + ")";
        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}