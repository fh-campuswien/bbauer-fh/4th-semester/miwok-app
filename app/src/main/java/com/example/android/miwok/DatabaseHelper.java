package com.example.android.miwok;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.android.miwok.models.TranslationModel;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
        super(context, DatabaseContract.DATABASE_NAME, null, DatabaseContract.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseContract.TranslationsTable.CREATE_TABLE);

        List<TranslationModel> translations = this.getTranslationsList();

        for (TranslationModel translation : translations) {
            ContentValues values = new ContentValues();
            values.put(DatabaseContract.TranslationsTable.DEFAULT_COL1, translation.getDefaultTranslationId());
            values.put(DatabaseContract.TranslationsTable.MIWOK_COL2, translation.getMiwokTranslationId());
            values.put(DatabaseContract.TranslationsTable.IMAGE_ID_COL3, translation.getDrawableId());
            values.put(DatabaseContract.TranslationsTable.CATEGORY_ID_COL4, translation.getCategoryId());
            db.insert(DatabaseContract.TranslationsTable.TABLE_NAME, null, values);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(DatabaseContract.TranslationsTable.DELETE_TABLE);
        onCreate(db);
    }

    private List<TranslationModel> getTranslationsList() {
        List<TranslationModel> translations = new ArrayList<>();

        translations.add(new TranslationModel(R.string.number_one, R.string.miwok_number_one, 0, R.drawable.number_one));
        translations.add(new TranslationModel(R.string.number_two, R.string.miwok_number_two, 0, R.drawable.number_two));
        translations.add(new TranslationModel(R.string.number_three, R.string.miwok_number_three, 0, R.drawable.number_three));
        translations.add(new TranslationModel(R.string.number_four, R.string.miwok_number_four, 0, R.drawable.number_four));
        translations.add(new TranslationModel(R.string.number_five, R.string.miwok_number_five, 0, R.drawable.number_five));
        translations.add(new TranslationModel(R.string.number_six, R.string.miwok_number_six, 0, R.drawable.number_six));
        translations.add(new TranslationModel(R.string.number_seven, R.string.miwok_number_seven, 0, R.drawable.number_seven));
        translations.add(new TranslationModel(R.string.number_eight, R.string.miwok_number_eight, 0, R.drawable.number_eight));
        translations.add(new TranslationModel(R.string.number_nine, R.string.miwok_number_nine, 0, R.drawable.number_nine));
        translations.add(new TranslationModel(R.string.number_ten, R.string.miwok_number_ten, 0, R.drawable.number_ten));

        translations.add(new TranslationModel(R.string.family_father, R.string.miwok_family_father, 1, R.drawable.family_father));
        translations.add(new TranslationModel(R.string.family_mother, R.string.miwok_family_mother, 1, R.drawable.family_mother));
        translations.add(new TranslationModel(R.string.family_son, R.string.miwok_family_son, 1, R.drawable.family_son));
        translations.add(new TranslationModel(R.string.family_daughter, R.string.miwok_family_daughter, 1, R.drawable.family_daughter));
        translations.add(new TranslationModel(R.string.family_older_brother, R.string.miwok_family_older_brother, 1, R.drawable.family_older_brother));
        translations.add(new TranslationModel(R.string.family_younger_brother, R.string.miwok_family_younger_brother, 1, R.drawable.family_younger_brother));
        translations.add(new TranslationModel(R.string.family_older_sister, R.string.miwok_family_older_sister, 1, R.drawable.family_older_sister));
        translations.add(new TranslationModel(R.string.family_younger_sister, R.string.miwok_family_younger_sister, 1, R.drawable.family_younger_sister));
        translations.add(new TranslationModel(R.string.family_grandmother, R.string.miwok_family_grandmother, 1, R.drawable.family_grandmother));
        translations.add(new TranslationModel(R.string.family_grandfather, R.string.miwok_family_grandfather, 1, R.drawable.family_grandfather));

        translations.add(new TranslationModel(R.string.color_red, R.string.miwok_color_red, 2, R.drawable.color_red));
        translations.add(new TranslationModel(R.string.color_mustard_yellow, R.string.miwok_color_mustard_yellow, 2, R.drawable.color_mustard_yellow));
        translations.add(new TranslationModel(R.string.color_dusty_yellow, R.string.miwok_color_dusty_yellow, 2, R.drawable.color_dusty_yellow));
        translations.add(new TranslationModel(R.string.color_green, R.string.miwok_color_green, 2, R.drawable.color_green));
        translations.add(new TranslationModel(R.string.color_brown, R.string.miwok_color_brown, 2, R.drawable.color_brown));
        translations.add(new TranslationModel(R.string.color_black, R.string.miwok_color_black, 2, R.drawable.color_black));
        translations.add(new TranslationModel(R.string.color_gray, R.string.miwok_color_gray, 2, R.drawable.color_gray));
        translations.add(new TranslationModel(R.string.color_white, R.string.miwok_color_white, 2, R.drawable.color_white));

        translations.add(new TranslationModel(R.string.phrase_where_are_you_going, R.string.miwok_phrase_where_are_you_going, 3));
        translations.add(new TranslationModel(R.string.phrase_what_is_your_name, R.string.miwok_phrase_what_is_your_name, 3));
        translations.add(new TranslationModel(R.string.phrase_my_name_is, R.string.miwok_phrase_my_name_is, 3));
        translations.add(new TranslationModel(R.string.phrase_how_are_you_feeling, R.string.miwok_phrase_how_are_you_feeling, 3));
        translations.add(new TranslationModel(R.string.phrase_im_feeling_good, R.string.miwok_phrase_im_feeling_good, 3));
        translations.add(new TranslationModel(R.string.phrase_are_you_coming, R.string.miwok_phrase_are_you_coming, 3));
        translations.add(new TranslationModel(R.string.phrase_yes_im_coming, R.string.miwok_phrase_yes_im_coming, 3));
        translations.add(new TranslationModel(R.string.phrase_im_coming, R.string.miwok_phrase_im_coming, 3));
        translations.add(new TranslationModel(R.string.phrase_lets_go, R.string.miwok_phrase_lets_go, 3));
        translations.add(new TranslationModel(R.string.phrase_come_here, R.string.miwok_phrase_come_here, 3));

        return translations;
    }

}
