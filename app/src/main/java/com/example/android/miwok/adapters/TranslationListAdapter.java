package com.example.android.miwok.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.miwok.R;
import com.example.android.miwok.models.TranslationModel;

import java.util.List;

public class TranslationListAdapter extends ArrayAdapter<TranslationModel> {
    public TranslationListAdapter(Context context, List<TranslationModel> translationModels) {
        super(context, 0, translationModels);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        TranslationModel translatedWord = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        // Lookup view for data population
        ImageView image = (ImageView) convertView.findViewById(R.id.image);
        TextView englishTranslation = (TextView) convertView.findViewById(R.id.default_text_view);
        TextView miwokTranslation = (TextView) convertView.findViewById(R.id.miwok_text_view);

        // Populate the data into the template view using the data object
        if (translatedWord.hasImage()) {
            image.setImageResource(translatedWord.getDrawableId());
            image.setVisibility(View.VISIBLE);
        } else {
            image.setVisibility(View.GONE);
            // ((ViewGroup)image.getParent()).removeView(image);
        }
        englishTranslation.setText(translatedWord.getDefaultTranslationId());
        miwokTranslation.setText(translatedWord.getMiwokTranslationId());

        // Return the completed view to render on screen
        return convertView;
    }
}