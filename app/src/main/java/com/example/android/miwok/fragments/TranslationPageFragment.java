package com.example.android.miwok.fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.android.miwok.DatabaseContract;
import com.example.android.miwok.DatabaseHelper;
import com.example.android.miwok.R;
import com.example.android.miwok.adapters.TranslationListAdapter;
import com.example.android.miwok.models.TranslationModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TranslationPageFragment extends Fragment {

    public List<TranslationModel> translations = new ArrayList<>();
    private static final HashMap<Integer, String> categories = new HashMap<Integer, String>() {{
        put(0, "Numbers");
        put(1, "Family");
        put(2, "Colors");
        put(3, "Phrases");
    }};

    public TranslationPageFragment() {
    }

    public static TranslationPageFragment newInstance(int page) {
        TranslationPageFragment fragmentFirst = new TranslationPageFragment();
        Bundle args = new Bundle();
        args.putInt("categoryId", page);
        fragmentFirst.setArguments(args);

        return fragmentFirst;
    }

    public static String getTitleByPageId(int page) {
        return categories.get(page);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        int categoryId = bundle.getInt("categoryId");

        DatabaseHelper dbHelper = new DatabaseHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query(
                DatabaseContract.TranslationsTable.TABLE_NAME,
                new String[]{
                        DatabaseContract.TranslationsTable.DEFAULT_COL1,
                        DatabaseContract.TranslationsTable.MIWOK_COL2,
                        DatabaseContract.TranslationsTable.IMAGE_ID_COL3,
                },
                DatabaseContract.TranslationsTable.CATEGORY_ID_COL4 + "=" + categoryId,
                null,
                null,
                null,
                null
        );

        while (cursor.moveToNext()) {
            this.translations.add(new TranslationModel(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    categoryId,
                    cursor.getInt(2)
            ));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_translations_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Create the adapter to convert the array to views
        TranslationListAdapter adapter = new TranslationListAdapter(this.getContext(), translations);
        // Attach the adapter to a ListView
        ListView listView = (ListView) view.findViewById(R.id.translations_list);
        listView.setAdapter(adapter);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
